<?php
    include('db_connections.php');
    include('session_init.php');

    $db = 'employees';
    $conn = mysql_connection($db);
    
    // Prepare query and bind variables
    $query = $conn->prepare("UPDATE employees SET itickets_password=:new_pass WHERE email_se=:session_name AND itickets_password=:current_pass LIMIT 1");
    $query->bindParam(':new_pass', $new_pass, PDO::PARAM_STR);
    $query->bindParam(':session_name', $email, PDO::PARAM_STR);
    $query->bindParam(':current_pass', $current_pass, PDO::PARAM_STR);
    
    
    $email = $_SESSION['username_link'] . '@santaeulalia.com';
    $new_pass1 = $_POST['newpassword1'];
    $new_pass2 = $_POST['newpassword2'];
    $current_pass = $_POST['currentpassword'];
    
    try {
        if($new_pass1 === $new_pass2) {
            $new_pass = $new_pass1;
            $query->execute();
        }
    }
    catch (PDOException $e) {
        echo 'No se pudo actualizar el registro: ' . $e->getMessage() . '<br>';
    }
    
    disconnect($conn);
    header("location:index.php");
