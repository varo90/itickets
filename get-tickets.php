<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('session_init.php');
    include('db_connections.php');
    include('session_init.php'); 
    if (((empty($_SESSION['username_link']) || !isset($_SESSION['username_link']))
            && basename($_SERVER['SCRIPT_FILENAME']) != 'login.php')) {
        header("location:login.php");
    } 
    
    $db = 'itickets';
    $conn = mysql_connection($db);
    
    $id_user = $_SESSION['userid_link'];
    $name_user = $_SESSION['username_link'];
    $group_user = $_SESSION['usergroup_link'];
    
    $es_admin = false;
    if($name_user == 'jbaladon' || $name_user == 'asantos') {
        $es_admin = true;
    }
    
    if($es_admin) {
        $select_emp = 'emp.full_name as employee,';
        $join_emp = ' LEFT JOIN employees.employees emp ON ti.user = emp.id';
        $cond = '';
    } else {
        $select_emp = '';
        $join_emp = '';
        $cond = " WHERE ti.user = $id_user";
    }
    
    $sql = "SELECT ti.id as id, $select_emp cat.name as category, ti.title as title, ti.description as description, ti.creation_date as creation_date, sta.id as id_status, sta.name as status, ti.last_updated as last_updated, ti.solution as solution, ti.attached
            FROM tickets ti
                LEFT JOIN categorias_ticket cat ON ti.category = cat.id
                LEFT JOIN status sta ON ti.status = sta.id" . $join_emp . $cond;
    
    $data = array();
    foreach ($conn->query($sql) as $row) {
        add_data($data,$row);
    }
    
    disconnect($conn);

    $results = array(
        "sEcho" => 1,
        "iTotalRecords" => count($data),
        "iTotalDisplayRecords" => count($data),
        "aaData"=>$data
    );

    echo json_encode($results);
    
    function add_data(&$data,$row) {
        $name_user = $_SESSION['username_link'];
        $ticket = $row['id'];
        if($name_user == 'jbaladon' || $name_user == 'asantos') {
            $nam = explode(' ', $row['employee']);
            $employee = $nam[0];
            if(count($nam) > 1) {
                 $employee = $nam[0] . ' ' . $nam[1];
            }
            $id_status = $row['id_status'];
            $stat = $row['status'];
            if($id_status == '1') {
                $options = "<li><a name=\"$ticket-proceso\" class=\"change\">Poner en proceso</a></li>
                            <li><a name=\"$ticket-cerrar\" class=\"change\">Cerrar</a></li>";
                $btn = 'btn-danger';
            } else if ($id_status == '2') {
                $options = "<li><a name=\"$ticket-abrir\" class=\"change\">Poner en abierto</a></li>
                            <li><a name=\"$ticket-cerrar\" class=\"change\">Cerrar</a></li>";
                $btn = 'btn-warning';
            } else if ($id_status == '3') {
                $options = "<li><a name=\"$ticket-abrir\" class=\"change\">Poner en abierto</a></li>
                            <li><a name=\"$ticket-proceso\" class=\"change\">Poner en proceso</a></li>";
                $btn = 'btn-success';
            } else {
                $options = '<li>---</li>';
                $btn = '';
            }
            $status = "<div id=\"$ticket\" class=\"dropdown dropup\">
                            <a class=\"dropdown-toggle btn btn-sm $btn\" data-toggle=\"dropdown\"><div class=\"hiden\">$id_status</div>$stat<b class=\"caret\"></b></a>
                            <ul class=\"dropdown-menu\" >
                              $options
                            </ul>
                       </div>";
            $sol = $row['solution'];
            $solution = "<div id=\"sol$ticket\">$sol</div>";
            $title = '<a id="'.$ticket.'" class="title_ticket">'.$row['title'].'</a>';
            $data[] = array('id'=>$ticket, 'employee'=>$employee, 'category'=>$row['category'], 'title'=>$title, 'creation_date'=>$row['creation_date'], 'status'=>$status, 'last_updated'=>$row['last_updated'], 'solution'=>$solution);
        } else {
            $id_status = $row['id_status'];
            $status = '<b>' . $row['status'] . '</b>';
            if($id_status == '1') {
                $status = "<font color=\"red\">$status</font>";
            } else if ($id_status == '2') {
                $status = "<font color=\"orange\">$status</font>";
            } else if($id_status == '3') {
                $status = "<font color=\"green\">$status</font>";
            }
            $title = '<a id="'.$ticket.'" class="title_ticket">'.$row['title'].'</a>';
            $data[] = array('id'=>$ticket, 'category'=>$row['category'], 'title'=>$title, 'creation_date'=>$row['creation_date'], 'status'=>$status, 'last_updated'=>$row['last_updated'], 'solution'=>$row['solution']);
        }
    }