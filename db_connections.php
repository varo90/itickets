<?php
    function mssql_connection_test() {
        $servername = 'srvsql';
        $db = 'SBO_EULALIA';
        $username = '******';
        $password = '******';
        
        return connect('ms',$servername,$db,$username,$password);
    }
    
    function mssql_connection_production() {
        $servername = 'srvsql';
        $db = 'SBO_EULALIA';
        $username='******';
        $password='******';
        
        return connect('ms',$servername,$db,$username,$password);
    }

    function mysql_connection($db) {
        $servername = 'localhost';
        $username = '******';
        $password = '******';
        
        return connect('my',$servername,$db,$username,$password);
    }
    
    function mysql_connection_jose() {
        $servername = 'smartapps.site';
        $db = 'warrick_SantaEulalia';
        $username = '******';
        $password = '******';
        
        return connect('my',$servername,$db,$username,$password);
    }
    
    function connect($type,$servername,$db,$username,$password) {
        try {
            if($type == 'ms') {
//                $mssqldriver = '{ODBC Driver 11 for SQL Server}';
//                $conn = new PDO("odbc:Driver=$mssqldriver;sqlsrv:Server=$servername;Database=$db", $username, $password);
                $conn = new PDO("sqlsrv:Server=$servername;Database=$db", $username, $password);
                $conn->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
            } else if ($type == 'my') {
                $conn = new PDO("mysql:host=$servername;dbname=$db", $username, $password);
            }
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "Connected successfully"; 
            return $conn;
        }
        catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    function disconnect(&$conn) {
        $conn = null;
    }