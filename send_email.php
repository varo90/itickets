<?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'On');
    include('session_init.php');
    include('/var/www/UTILS/PHPMailer/src/SMTP.php');
    include('/var/www/UTILS/PHPMailer/src/PHPMailer.php');
    
    function prepare_mail_it($type,$message) {
        $to = array('asantos@santaeulalia.com','jbaladon@santaeulalia.com');
        $subject = 'Nuevo ticket de ' . $_SESSION['username_link'] . ': ' . $type;
        $from = "it@santaeulalia.com";
        
        send_mail($to,$subject,$message);
    }
    
    function prepare_mail_user($status,$ticket) {
        $info_ticket = get_info_ticket($ticket);
        if($status == 1) {
            $mail_address = $_SESSION['username_link'] . '@santaeulalia.com';
        } else {
            $mail_address = $info_ticket['email'];
        }
        $to = array($mail_address);
        $datetime = date_create_from_format('Y-m-d H:i:s',$info_ticket['creation_date']);
        $date = date_format($datetime, 'd-m-Y');
        $time = date_format($datetime, 'H:i:s');
        if($status == 1) {
            $subject = 'Nueva solicitud abierta para IT: ' . $info_ticket['category'];
            $message = 'Usted ha abierto la siguiente solicitud:<br><br><br>' .
                        $info_ticket['category'] . '<br><br>' .
                        $info_ticket['description'] . '<br><br><br>' .
                        'Procesaremos su solicitud a la mayor prontitud y le mantendremos informado.' . '<br>' .
                        'Gracias y un saludo.';
        } else if($status == 2) {
            $subject = 'Estado de la solicitud con id ' . $info_ticket['id_ticket'] . ': ' . $info_ticket['category'];
            $message = 'Le informamos de que su solicitud abierta el ' . $date . ' a las ' . $time . ' ha pasado a estar en proceso:<br><br><br>' .
                        $info_ticket['category'] . '<br><br>' .
                        $info_ticket['description'] . '<br><br><br>' .
                        'Esto significa que estamos trabajando activamente en el asunto y obtendr&aacute; una respuesta a la mayor prontitud.' . '<br>' .
                        'Gracias y un saludo.';
        } else if($status == 3) {
            $subject = 'Solicitud con id ' . $info_ticket['id_ticket'] . ' resuelta: ' . $info_ticket['category'];
            $message = 'Le informamos de que su solicitud abierta el ' . $date . ' a las ' . $time . ' ha sido resuelta:<br><br><br>' .
                        $info_ticket['category'] . '<br><br>' .
                        $info_ticket['description'] . '<br><br><br>';
            $solution = $info_ticket['solution'];
            if($solution != NULL && $solution != '' && strlen($solution)>0) {
                $message = $message . 'La soluci&oacute;n aplicada ha sido la siguiente: ' . '<br><br>' .
                $solution . '<br><br>' .
                'Gracias y un saludo.';
            }
        }
        $subject = $subject;
        $message = $message;
        
        send_mail($to,$subject,$message);
    }
    
    function send_mail($to,$subject,$message) {
        
        $mail = new PHPMailer(true);
        //Luego tenemos que iniciar la validación por SMTP:
        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = 'smtp.office365.com'; // Aquí pondremos el SMTP a utilizar. Por ej. mail.midominio.com
        $mail->Username = 'it@santaeulalia.com'; // Email de la cuenta de correo. ej.info@midominio.com La cuenta de correo debe ser creada previamente. 
        $mail->Password = 'Informatica2013'; // Aqui pondremos la contraseña de la cuenta de correo
        $mail->Port = 587; // Puerto de conexión al servidor de envio. 
        $mail->From = 'it@santaeulalia.com'; // Desde donde enviamos (Para mostrar). Puede ser el mismo que el email creado previamente.
        $mail->FromName = 'Solicitudes IT Santa Eulalia'; //Nombre a mostrar del remitente. 
        foreach($to as $mail_address) {
            $mail->AddAddress($mail_address); // Esta es la dirección a donde enviamos 
        }
        $mail->IsHTML(true); // El correo se envía como HTML 
        $mail->Subject = utf8_decode($subject); // Este es el titulo del email. 
        $body = '<font style="font:15px Arial;">' . utf8_decode($message) . '<br>'; 
        $mail->Body = $body; // Mensaje a enviar. 
        try {
            $mail->Send(); // Envía el correo.
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }
    
    function get_info_ticket($ticket) {

        $db = 'itickets';
        $conn = mysql_connection($db);

        // Prepare query and bind variables
        $query = $conn->prepare("SELECT t.id as id_ticket, emp.email_se as email, ct.name as category, t.creation_date as creation_date, t.last_updated as last_updated, t.description as description, t.solution as solution
                                 FROM tickets t 
                                      LEFT JOIN categorias_ticket ct ON t.category = ct.id
                                      LEFT JOIN employees.employees emp ON t.user = emp.id
                                 WHERE t.id=:ticket
                                 LIMIT 1");
        $query->bindParam(':ticket', $ticket, PDO::PARAM_STR);

        try {
            $query->execute();
            if($query->rowCount() > 0) {
                $result = $query->fetchAll()[0];
                return $result;
            }
        }
        catch (PDOException $e) {
            echo 'No se pudo obtener la informaci&oacute;n.' . $e->getMessage() . '<br>';
        }

        disconnect($conn);
    }
    

