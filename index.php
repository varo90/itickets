<?php
    include('session_init.php');
    include('db_connections.php');
    
    if (empty($_SESSION['username_link']) || !isset($_SESSION['username_link'])) {
        header("location:login.php");
    }

    if($_SESSION['usergroup_link'] == 1 || $_SESSION['usergroup_link'] == 2) {
        header("location:solicitudes.php");
    } else {
        header("location:login.php");
    }