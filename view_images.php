<?php
    $n_ticket = $_GET['ticket'];
    $directory = 'uploaded_pics/';
    
    // Get jpg files ordered by date
    chdir($directory);
    array_multisort(array_map('filemtime', ($files = glob("*"))), SORT_DESC, $files);
    
    foreach($files as $file) {
        if(strpos($file, $n_ticket) !== false) {
            $file_route = $directory.$file;
            echo "<img src='$file_route'>";
        }
    }
?>
