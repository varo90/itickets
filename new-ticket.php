<?php
    include('header.php');
    include('db_connections.php');
    include('session_init.php');

    $db = 'itickets';
    $conn = mysql_connection($db);
    
    $id_user = $_SESSION['userid_link'];
    
    $sql = "SELECT * FROM categorias_ticket cat";
    
?>

    <div class='contenedor'>
        <center>
            <header>
                <h1>Nueva solicitud para IT</h1>
            </header>
        </center>
        <form enctype="multipart/form-data" action="register-new-ticket.php" method="POST">
            <label>Seleccione el tipo de solicitud:</label>
            <select class="form-control" name="category">
            <?php
                foreach ($conn->query($sql) as $row) {
                    if($row[0] == '1') {
                        $selected = 'selected';
                    } else {
                        $selected = '';
                    }
            ?>
                    <option value="<?php echo $row[0] ?>" <?php echo $selected; ?>><?php echo $row[1] ?></option>
            <?php
                }
            ?>
            </select>
            <br>
            <div class="form-group">
                <input type="text" class="form-control" name="title" placeholder="Introduce un t&iacute;tulo o descrici&oacute;n corta" required>
            </div>
            <br>
            <div class="form-group">
<!--                <label>Describa su petición:</label>-->
                <textarea id="description_ticket" maxlength="50" name="description" class="form-control" rows="10"></textarea>
            </div>
            <br>
            <div class="form-group">
                <label>Selecciona uno o varios archivos o imágenes:</label>
                <input type="file" name="imgs[]" multiple>
            </div>
            <a href="index.php" class="btn btn-info btn-lg back" role="button" aria-pressed="true">Volver</a>
            <input style="float:right;cursor:pointer;" type="submit" id="send_ticket" class="btn btn-primary btn-lg" role="button" aria-pressed="true">
        </form>
    </div>

<?php
    disconnect($conn);
?>