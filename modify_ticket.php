<?php    
    include('session_init.php');
    include('db_connections.php');
    include('send_email.php');
    
    $db = 'itickets';
    $conn = mysql_connection($db);
    
    $solution = $_POST['solu'];
    if($solution != '') {
        $solu = ', solution=:solution';
    } else {
        $solu = '';
    }
    
    // Prepare query and bind variables
    $query = $conn->prepare("UPDATE tickets SET status=:option $solu WHERE id=:ticket");
    $query->bindParam(':option', $option, PDO::PARAM_STR);
    $query->bindParam(':ticket', $ticket, PDO::PARAM_STR);
    if($solution != '') {
        $query->bindParam(':solution', $solution, PDO::PARAM_STR);
    }
    
    $op = $_POST['op'];
    $ticket = $_POST['ntick'];
    if($op == 'abrir') {
        $option = 1;
        $options = "<li><a name=\"$ticket-proceso\" class=\"change\">Poner en proceso</a></li>
                    <li><a name=\"$ticket-cerrar\" class=\"change\">Cerrar</a></li>";
        $btn = 'btn-danger';
        $stat = 'Abierto';
    } else if ($op == 'proceso') {
        $option = 2;
        $options = "<li><a name=\"$ticket-abrir\" class=\"change\">Poner en abierto</a></li>
                    <li><a name=\"$ticket-cerrar\" class=\"change\">Cerrar</a></li>";
        $btn = 'btn-warning';
        $stat = 'En proceso';
    } else if ($op == 'cerrar') {
        $option = 3;
        $options = "<li><a name=\"$ticket-abrir\" class=\"change\">Poner en abierto</a></li>
                    <li><a name=\"$ticket-proceso\" class=\"change\">Poner en proceso</a></li>";
        $btn = 'btn-success';
        $stat = 'Cerrado';
    } else {
        $option = 4;
        $options = '<li>---</li>';
        $btn = '';
        $stat = '';
    }
    $id_status = $option;
    
    try {
        if($query->execute()) {
            echo "<div id=\"$ticket\" class=\"dropdown dropup\">
                    <a class=\"dropdown-toggle btn btn-sm $btn\" data-toggle=\"dropdown\"><div class=\"hiden\">$id_status</div>$stat<b class=\"caret\"></b></a>
                    <ul class=\"dropdown-menu\" >
                      $options
                    </ul>
                 </div>";
            if($option == 2 || $option == 3) {
                prepare_mail_user($option,$ticket);
            }
        }
    }
    catch (PDOException $e) {
        echo 'No se pudo crear el registro: ' . $e->getMessage() . '<br>';
    }
    
    disconnect($conn);