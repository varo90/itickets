<?php
    include('header.php');
    $es_admin = false;
    if(strtolower($name_user) == 'jbaladon' || strtolower($name_user) == 'asantos') {
        $es_admin = true;
    }
?>
    <div class="contenedor">
        <table id="data-info" class="display" width="100%" cellspacing="0">
            <thead>
                <tr>
                    <th style="width: 25px;">Tkt</th>
                    <?php
                    if($es_admin) { 
                    ?>
                        <th style="width: 60px;">User</th>
                    <?php
                    } else {
                    }
                    ?>
                    <th style="width: 80px;">Tipo</th>
                    <th style="width: 520px;">Solicitud</th>
                    <th style="width: 85px;">Fecha de Apertura</th>
                    <th style="width: 85px;">&Uacute;ltima actualizaci&oacute;n</th>
                    <th>Estado</th>
                    <th style="width: 320px;">Soluci&oacute;n</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Tkt</th>
                    <?php
                    if($es_admin) { 
                    ?>
                        <th>User</th>
                    <?php
                    }
                    ?>
                    <th>Tipo</th>
                    <th>Solicitud</th>
                    <th>Fecha de Apertura</th>
                    <th>Estado</th>
                    <th>&Uacute;ltima actualizaci&oacute;n</th>
                    <th style="width: 120px;">Soluci&oacute;n</th>
                </tr>
            </tfoot>
        </table>
        <br>
        <div id="updated"></div>
        <a id='open_new' href="new-ticket.php" class="btn btn-primary btn-lg" role="button" aria-pressed="true">Nueva solicitud</a>
    </div>

    <script type="text/javascript" language="javascript" class="init">
        $( document ).ready(function() {
            $('#data-info').dataTable({
                "bProcessing": true,
                "sAjaxSource": "get-tickets.php",
                "aaSorting": [[6,'asc'],[0,'desc']],
                "lengthMenu": [[50, 100, 150, 200], [50, 100, 150, 200]],
                "oLanguage": {
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
                    "sSearch": "Buscar:",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    }
                },
                "aoColumns": [
                    { mData: 'id' },
                    <?php
                    if($es_admin) { 
                    ?>
                    { mData: 'employee' },
                    <?php
                    }
                    ?>
                    { mData: 'category' },
                    { mData: 'title' },
                    { mData: 'creation_date' },
                    { mData: 'last_updated' },
                    { mData: 'status' },
                    { mData: 'solution' }
                ]
            });
        });
        
        $(document).on('click', "a.change", cambiar);
        $(document).on('click', ".title_ticket", see_desc);
            
        function cambiar() {
            var name = $(this).attr("name").split('-');
            var ntick = name[0];
            var op = name[1];
            solu = '';
            if(op === 'cerrar') {
                bootbox.prompt({
                    title: "Escribe la solución",
                    inputType: 'textarea',
                    callback: function (result) {
                        call_ajax(ntick,op,result);
                        $("#sol" + ntick).text(result);
                    }
                });
            } else {
                call_ajax(ntick,op,solu);
            }
        }
            
        
        function call_ajax(ntick,op,solu) {
            var nlabel = '#' + ntick;
            var data = {
                "op" : op,
                "ntick" : ntick,
                "solu" : solu
            };
            $.ajax({
                data:  data,
                url:   'modify_ticket.php',
                type:  'post',
                beforeSend: function () {
                        $(nlabel).html("Actualizando...");
                },
                success:  function (response) {
                    $(nlabel).html(response);
                }
            });
        }
        
        function see_desc() {
            var id = $(this).attr('id');
            var text = $(this).html();
            var data = {
                "id" : id
            };
            $.ajax({
                data:  data,
                url:   'get_description.php',
                type:  'post',
                success:  function (response) {
                    bootbox.confirm({
                        title: text,
                        message: response,
//                        buttons: {
//                            cancel: {
//                                label: '<i class="fa fa-times"></i> Cerrar'
//                            }
//                        },
                        callback: function (result) {
                            console.log('This was logged in the callback: ' + result);
                        }
                    });
                }
            });
            
        }
        
    </script>
</body>
</html>