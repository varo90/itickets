<?php
    include('db_connections.php');
    include('session_init.php');
    include('send_email.php');
    
    $db = 'itickets';
    $conn = mysql_connection($db);
    
    // Prepare query and bind variables
    $query = $conn->prepare("INSERT INTO tickets (user,category,creation_date,status,title,description,attached) VALUES (:user,:category,:creation_date,:status,:title,:description,:attached)");
    $query->bindParam(':user', $id_user, PDO::PARAM_STR);
    $query->bindParam(':category', $category_id, PDO::PARAM_STR);
    $query->bindParam(':creation_date', $current_date, PDO::PARAM_STR);
    $query->bindParam(':status', $status, PDO::PARAM_STR);
    $query->bindParam(':title', $title, PDO::PARAM_STR);
    $query->bindParam(':description', $description, PDO::PARAM_STR);
    $query->bindParam(':attached', $countfiles, PDO::PARAM_STR);
        
    $id_user = $_SESSION['userid_link'];
    $category_id = $_POST['category'];
    $current_date = date('Y-m-d H:i:s');
    $status = 1;
    $title = $_POST['title'];
    $description = $_POST['description'];
    $countfiles = sizeof($_FILES['imgs']['name']);
    
    try {
        if($query->execute()) {
            $ticket = $conn->lastInsertId();
            upload_files($ticket);
            prepare_mail_user($status,$ticket);
            $sql = "SELECT * FROM categorias_ticket cat WHERE cat.id=$category_id LIMIT 1";
            foreach($conn->query($sql) as $row) {
                $category = $row['name'];
            }
            prepare_mail_it($category,$description);
            disconnect($conn);
            header("location:index.php");
        }
    }
    catch (PDOException $e) {
        echo 'No se pudo crear el registro: ' . $e->getMessage() . '<br>';
    }
        
    disconnect($conn);
    
    
    function upload_files($ticket) {
        $countfiles = count($_FILES['imgs']['name']);
        if($countfiles > 0) {
            // Looping all files
            for($i=0;$i<$countfiles;$i++){
                $img_name = $_FILES['imgs']['name'][$i];
                $img_parts = explode('.',$img_name);
                $extension = end($img_parts);
                $filename = $ticket . '_' . $i . '.' . $extension;
                // Upload file
                move_uploaded_file($_FILES['imgs']['tmp_name'][$i],'uploaded_pics/'.$filename);
            }
            return TRUE;
        }
        return FALSE;
    }
